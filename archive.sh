#!/bin/bash
# Остановка выполнения скрипта в случае ошибок, при работе в windows постоянно не тот код ошибки:
#set -euo pipefail
# Именование файлов по дате изменения см test.sh 
# у меня не склеивались в одну строку время и дата из-за переноса строки в конце переменной,
# способ как это побороть не нашел, в дальнейшем пользуюсь текущим временем и датой. 
echo "Архивирование папки с логами"
logpath=outbox/$(date  +%Y-%m-%d)/;
logfile=$(date  +%H-%M-%S);
mkdir -p $logpath|
tar -czf "$logpath$logfile.tar" inbox
echo "Поиск логов с ошибками"
grep -r -i -h --include=*.log '^error' inbox  > "$logpath$logfile"-errors.txt|
echo "удаление файлов в папке inbox"
rm -rf inbox/
echo $?